# HACKERNEWS-NODE

This project is a try-out when learning graphQL basics.

I backed my learning journey mostly by digging into the [GraphQL spec](https://graphql.org/learn/).

And the really good content on [How to GraphQL](https://www.howtographql.com/): practical tutorials and explanations.

And also [Prisma Blog](https://www.prisma.io/blog/) which provides in-depth articles to understand GraphQL under the hood.

I learned a lot watching that video:

[![That video](http://img.youtube.com/vi/fo6X91t3O2I/0.jpg)](http://www.youtube.com/watch?v=fo6X91t3O2I)

My project is linted with the [eslint-config-airbnb](https://github.com/airbnb/javascript/tree/master/packages/eslint-config-airbnb).

The GraphQL server is implemented using [graphql-yoga](https://github.com/prisma-labs/graphql-yoga) which provides a very pleasant experience for a first enconter with GraphQL.

The ORM that provides an API between the GraphQL server API and the database is: [Prisma@1.34](https://v1.prisma.io/docs/1.34).

Note that, I used a demo database in order to experiment without having to do a complete local setup.
You can learn more on that [here](https://v1.prisma.io/docs/1.34/get-started/01-setting-up-prisma-demo-server-JAVASCRIPT-a001/).

## Running the project

Clone / Fork the project and then run **npm install** in order to fetch all dependencies.

Then populate a **_.env_** file with the variable: **ENDPOINT_URL**=Your_database_endpoint_url.

_Please note, that this works only with a cloud-base relational database._
_If you want to use it with a local database of any kind, please refer to [Prisma@1.34 official documentation](https://v1.prisma.io/docs/)._

When you're done run the following command:

**prisma deploy**

**Note that Prisma is installed as a dev dependancy, so you might need to prepend your command with _npx_**

You can then run **npm start**, which will start the server using **[nodemon](https://github.com/remy/nodemon)** to monitor changes and avoid having to manually restart the application.
